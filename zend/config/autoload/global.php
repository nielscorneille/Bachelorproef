<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
	// ...
	'navigation'      => array(
		'default' => array(
			array(
				'label'     => 'Home',
				'route'     => 'home',
				'resource'  => 'Application\Controller\Index',
				'privilege' => 'home'
			),
			array(
				'label'     => 'About',
				'route'     => 'about',
				'resource'  => 'Application\Controller\Index',
				'privilege' => 'about'

			),
			array(
				'label'     => 'Contact',
				'route'     => 'contact',
				'resource'  => 'Application\Controller\Index',
				'privilege' => 'contact'

			),
			array(
				'label'     => 'Login',
				'route'     => 'login',
				'resource'  => 'Application\Controller\Auth',
				'privilege' => 'login'
			),
			array(
				'label'     => 'Logout',
				'route'     => 'logout',
				'resource'  => 'Application\Controller\Auth',
				'privilege' => 'logout'
			)
		),
	),
	'db'              => array(
		'driver'         => 'Pdo',
		'dsn'            => 'mysql:dbname=zend_site;host=localhost',
		'driver_options' => array(
			PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
		),
	),
	'vars'            => array(
		'emailTo' => 'nielscor@gmail.com',
		'nameTo'  => 'Niels',
		'salt' => '$2y$14$aNOWwDqbvZ2ll2L8ayXeMOG3TantRTS9btMze6QU0ODdlmEEBLDzq',
	),
	'service_manager' => array(
		'factories' => array(
			'navigation'              => 'Zend\Navigation\Service\DefaultNavigationFactory',
			'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
		),
	),

);
