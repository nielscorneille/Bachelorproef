<?php
namespace Blog;

use Blog\Model\Post;
use Blog\Model\Comment;
use Blog\Model\Tag;
use Blog\Model\PostTable;
use Blog\Model\CommentTable;
use Blog\Model\TagTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
