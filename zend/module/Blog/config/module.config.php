<?php
namespace Blog;
return array(
	'controllers'  => array(
		'invokables' => array(
			'Blog\Controller\Post'    => 'Blog\Controller\PostController',
			'Blog\Controller\Comment' => 'Blog\Controller\CommentController',
		),
	),

	// The following section is new and should be added to your file
	'router'       => array(
		'routes' => array(
			'post'    => array(
				'type'    => 'segment',
				'options' => array(
					'route'       => '/blog[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'     => '[0-9]+',
					),
					'defaults'    => array(
						'controller' => 'Blog\Controller\Post',
						'action'     => 'index',
					),
				),
			),
			'comment' => array(
				'type'    => 'segment',
				'options' => array(
					'route'       => '/comment[/:action][/:id]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'     => '[0-9]+',
					),
					'defaults'    => array(
						'controller' => 'Blog\Controller\Comment',
						'action'     => 'index',
					),
				),
			),
		),
	),

	'view_manager' => array(
		'template_path_stack' => array(
			'blog' => __DIR__ . '/../view'
		),
	),
	'doctrine'     => array(
		'driver' => array(
			__NAMESPACE__ . '_driver' => array(
				'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
				'cache' => 'array',
				'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Model')
			),

			'orm_default'          => array(
				'drivers' => array(
					__NAMESPACE__ . '\Model' =>  __NAMESPACE__ . '_driver'
				)
			))),
);