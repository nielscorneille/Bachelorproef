<?php
namespace Blog\Controller;

use Blog\Form\CommentForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class CommentController extends AbstractActionController
{
	protected $commentTable;
	public function indexAction()
	{
		return new ViewModel(array(
			'comments' => $this->getCommentTable()->fetchAll(),
		));
	}

	public function addAction()
	{
		$form = new CommentForm();
		$form->get('submit')->setValue('Add');

		$request = $this->getRequest();
		if ($request->isComment()) {
			$comment = new Comment();
			$form->setInputFilter($comment->getInputFilter());
			$form->setData($request->getComment());

			if ($form->isValid()) {
				$comment->exchangeArray($form->getData());
				$this->getCommentTable()->saveComment($comment);

				// Redirect to list of albums
				return $this->redirect()->toRoute('comment');
			}
		}
		return array('form' => $form);
	}

	public function editAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('comment', array(
				'action' => 'add'
			));
		}

		// Get the Album with the specified id.  An exception is thrown
		// if it cannot be found, in which case go to the index page.
		try {
			$comment = $this->getCommentTable()->getComment($id);
		}
		catch (\Exception $ex) {
			return $this->redirect()->toRoute('comment', array(
				'action' => 'index'
			));
		}

		$form  = new CommentForm();
		$form->bind($comment);
		$form->get('submit')->setAttribute('value', 'Edit');

		$request = $this->getRequest();
		if ($request->isComment()) {
			$form->setInputFilter($comment->getInputFilter());
			$form->setData($request->getComment());

			if ($form->isValid()) {
				$this->getCommentTable()->saveComment($form->getData());

				// Redirect to list of albums
				return $this->redirect()->toRoute('comment');
			}
		}

		return array(
			'id' => $id,
			'form' => $form,
		);
	}

	public function deleteAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('comment');
		}

		$request = $this->getRequest();
		if ($request->isComment()) {
			$del = $request->getComment('del', 'No');

			if ($del == 'Yes') {
				$id = (int) $request->getComment('id');
				$this->getCommentTable()->deleteComment($id);
			}

			// Redirect to list of albums
			return $this->redirect()->toRoute('comment');
		}

		return array(
			'id'    => $id,
			'comment' => $this->getCommentTable()->getComment($id)
		);
	}

	public function getCommentTable()
	{
		if (!$this->commentTable) {
			$sm = $this->getServiceLocator();
			$this->commentTable = $sm->get('Blog\Model\CommentTable');
		}
		return $this->commentTable;
	}
}