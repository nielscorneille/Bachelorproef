<?php
namespace Blog\Controller;

use Blog\Form\PostForm;

use Blog\Model\Tag;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class PostController extends AbstractActionController
{


	public function indexAction()
	{
		return new ViewModel(array(
			'posts' => $this->getObjectManager()->getRepository('Blog\Model\Post')->findAll()
		));
	}

	public function addAction()
	{
		$form = new PostForm();
		$form->get('submit')->setValue('Add');

		$request = $this->getRequest();
		if ($request->isPost())
		{
			$post = new Post();
			$form->setInputFilter($post->getInputFilter());
			$form->setData($request->getPost());

			if ($form->isValid())
			{
				$post->exchangeArray($form->getData());
				$this->getObjectManager()->persist($post);
				$this->getObjectManager()->flush();

				// Redirect to list of albums
				return $this->redirect()->toRoute('post');
			}
		}
		return array('form' => $form);
	}

	public function editAction()
	{
		$id = (int)$this->params()->fromRoute('id', 0);
		if (!$id)
		{
			return $this->redirect()->toRoute('post', array(
				'action' => 'add'
			));
		}

		// Get the Album with the specified id.  An exception is thrown
		// if it cannot be found, in which case go to the index page.
		try
		{
			$post = $this->getPostTable()->getPost($id,$this->getTagTable(),$this->getCommentTable());
		}
		catch (\Exception $ex)
		{
			return $this->redirect()->toRoute('post', array(
				'action' => 'index'
			));
		}

		$form = new PostForm();
		$form->bind($post);
		$form->get('submit')->setAttribute('value', 'Edit');

		$request = $this->getRequest();
		if ($request->isPost())
		{
			$form->setInputFilter($post->getInputFilter());
			$form->setData($request->getPost());

			if ($form->isValid())
			{
				$this->getObjectManager()->persist($post);
				$this->getObjectManager()->flush();

				// Redirect to list of albums
				return $this->redirect()->toRoute('post');
			}
		}

		return array(
			'id'   => $id,
			'form' => $form,
		);
	}

	public function deleteAction()
	{
		$id = (int)$this->params()->fromRoute('id', 0);
		if (!$id)
		{
			return $this->redirect()->toRoute('post');
		}

		$request = $this->getRequest();
		if ($request->isPost())
		{
			$del = $request->getPost('del', 'No');

			if ($del == 'Yes')
			{
				$id = (int)$request->getPost('id');
				$this->getObjectManager()->remove($this->getObjectManager()->find('Blog\Model\Post', $id));
				$this->getObjectManager()->flush();
			}

			// Redirect to list of albums
			return $this->redirect()->toRoute('post');
		}

		return array(
			'id'   => $id,
			'post' => $this->getObjectManager()->find('Blog\Model\Post', $id)
		);
	}
	public function viewAction()
	{
		$id = (int)$this->params()->fromRoute('id', 0);
		if (!$id)
		{
			return $this->redirect()->toRoute('post');
		}
		$post = $this->getObjectManager()->find('Blog\Model\Post', $id);
		$comments = $this->getObjectManager()->getRepository('Blog\Model\Comment')
			->findBy(array('post' => $post));
		return array(
			'id'   => $id,
			'post' => $post,
			'comments' => $comments,
		);
	}


	public function getObjectManager()
	{
		return $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
	}
}