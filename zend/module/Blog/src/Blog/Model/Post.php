<?php
namespace Blog\Model;
use Doctrine\ORM\Mapping as ORM;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\TableGateway\TableGateway;

/**
 * TblPost
 *
 * @ORM\Table(name="tbl_post")
 * @ORM\Entity
 */
class Post
{
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @param \Blog\Model\Application\Model\User $author
	 */
	public function setAuthor($author)
	{
		$this->author = $author;
	}

	/**
	 * @return \Blog\Model\Application\Model\User
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * @param string $content
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}

	/**
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param int $createTime
	 */
	public function setCreateTime($createTime)
	{
		$this->createTime = $createTime;
	}

	/**
	 * @return int
	 */
	public function getCreateTime()
	{
		return $this->createTime;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return int
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param string $tags
	 */
	public function setTags($tags)
	{
		$this->tags = $tags;
	}

	/**
	 * @return string
	 */
	public function getTags()
	{
		return $this->tags;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param int $updateTime
	 */
	public function setUpdateTime($updateTime)
	{
		$this->updateTime = $updateTime;
	}

	/**
	 * @return int
	 */
	public function getUpdateTime()
	{
		return $this->updateTime;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="title", type="string", length=128, nullable=false)
	 */
	private $title;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="content", type="text", nullable=false)
	 */
	private $content;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="tags", type="text", nullable=true)
	 */
	private $tags;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="status", type="integer", nullable=false)
	 */
	private $status;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="create_time", type="integer", nullable=true)
	 */
	private $createTime;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="update_time", type="integer", nullable=true)
	 */
	private $updateTime;

	/**
	 * @var Application\Model\User
	 *
	 * @ORM\ManyToOne(targetEntity="Application\Model\User")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="author_id", referencedColumnName="id")
	 * })
	 */
	private $author;

	protected $inputFilter;

	public function exchangeArray($data)
	{
		$this->id      = (isset($data['id'])) ? $data['id'] : NULL;
		$this->title   = (isset($data['title'])) ? $data['title'] : NULL;
		$this->content = (isset($data['content'])) ? $data['content'] : NULL;
		$this->tags    = (isset($data['tags'])) ? $data['tags'] : NULL;
		$this->status  = (isset($data['status'])) ? $data['status'] : NULL;
		$this->update_time  = (isset($data['update_time'])) ? $data['update_time'] : NULL;
	}

	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}

	public function getInputFilter()
	{
		if (!$this->inputFilter)
		{
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();

			$inputFilter->add($factory->createInput(array(
				'name'     => 'id',
				'required' => TRUE,
				'filters'  => array(
					array('name' => 'Int'),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'       => 'title',
				'required'   => TRUE,
				'filters'    => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'       => 'content',
				'required'   => TRUE,
				'filters'    => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 300,
						),
					),
				),
			)));

			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}

	public function getArrayCopy()
	{
		return get_object_vars($this);
	}

}
