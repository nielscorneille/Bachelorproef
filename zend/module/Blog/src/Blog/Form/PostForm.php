<?php
namespace Blog\Form;

use Zend\Form\Form;

class PostForm extends Form
{
	public function __construct($name = NULL)
	{
		// we want to ignore the name passed
		parent::__construct('post');
		$this->setAttribute('method', 'post');
		/*
				<div class="control-group">

				<label class="control-label" for="Post_tags">Tags</label>		<div class="controls">
				<input size="50" id="Post_tags" name="Post[tags]" type="text" value="test, blog" />		<p class="hint">Please separate different tags with commas.</p>
				<span class="help-inline error" id="Post_tags_em_" style="display: none"></span>	</div>
			</div>*/
		$this->add(array(
			'name' => 'id',
			'type' => 'Hidden',
		));
		$this->add(array(
			'name'    => 'title',
			'type'    => 'Text',
			'options' => array(
				'label' => 'Title',

			),
		));
		$this->add(array(
			'name'    => 'content',
			'type'    => 'Textarea',
			'options' => array(
				'label'    => 'Content',
				'required' => TRUE,
			),
		));
		$this->add(array(
			'name'    => 'tags',
			'type'    => 'Text',
			'options' => array(
				'label'       => 'Tags',
				'description' => 'Please separate different tags with commas.',

			),
		));
		$this->add(array(
			'name'    => 'status',
			'type'    => 'Select',
			'options' => array(
				'label'         => 'Status',
				'value_options' => array(
					1 => 'Draft',
					2 => 'Published',
					3 => 'Archived',
				),
			),
		));
		$this->add(array(
			'name'       => 'submit',
			'type'       => 'Submit',
			'attributes' => array(
				'value' => 'Go',
				'id'    => 'submitbutton',
			),
			'options'    => array(
				'primary' => TRUE,
			),
		));
	}
}
