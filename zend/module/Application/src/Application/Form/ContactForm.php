<?php
namespace Application\Form;


use Zend\Captcha\AdapterInterface as CaptchaAdapter;
use Zend\Captcha\Dumb;
use Zend\Captcha;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\Validator\EmailAddress;
use \Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use \Zend\InputFilter\InputFilterAwareInterface;
use \Zend\InputFilter\InputFilterInterface;

class ContactForm extends Form implements InputFilterAwareInterface
{
	protected $captcha;

	public function setCaptcha(CaptchaAdapter $captcha)
	{
		$this->captcha = $captcha;
	}

	public function __construct($name = NULL)
	{
		//pass captcha image options
		$this->captcha = new Dumb();
		//$this->captcha->setLabel('Please verify you are human');
		// we want to ignore the name passed
		parent::__construct('contact');
		$this->setAttribute('method', 'post');
		$this->add(array(
			'name'       => 'name',
			'options'    => array(
				'label' => 'Name',
			),
			'attributes' => array(
				'type' => 'text',
			),
		));
		$this->add(array(
			//	'type'    => 'Zend\Form\Element\Email',
			'name'       => 'email',
			'options'    => array(
				'label' => 'Email',
			),
			'attributes' => array(
				'type' => 'text',
			),
		));
		$this->add(array(
			'name'       => 'subject',
			'options'    => array(
				'label' => 'Subject',
			),
			'attributes' => array(
				'type' => 'text',
			),
		));
		$this->add(array(
			'type'    => 'Zend\Form\Element\Textarea',
			'name'    => 'message',
			'options' => array(
				'label' => 'Message',
			),
		));
		$this->add(array(
			'type'    => 'Zend\Form\Element\Captcha',
			'name'    => 'captcha',
			'options' => array(
				'label'   => 'Verification code',
				'captcha' => $this->captcha,
			),
		));
		$this->add(new Element\Csrf('security'));
		$this->add(array(
			'name'       => 'send',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Submit',
			),
		));

	}

	protected $inputFilter;

	// Add content to these methods:
	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}

	public function getInputFilter()
	{
		if (!$this->inputFilter)
		{
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();

			$inputFilter->add($factory->createInput(array(
				'name'       => 'name',
				'required'   => TRUE,
				'filters'    => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'       => 'email',
				'required'   => TRUE,
				'filters'    => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'EmailAddress',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 5,
							'max'      => 255,
						),
					),
				)
			)));
			$inputFilter->add($factory->createInput(array(
				'name'       => 'subject',
				'required'   => FALSE,

				'filters'    => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 0,
							'max'      => 100,
						),
					),
				),
			)));
			$inputFilter->add($factory->createInput(array(
				'name'       => 'message',
				'required'   => TRUE,
				'filters'    => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 500,
						),
					),
				),
			)));
			$inputFilter->add($this->get('captcha')->getInputSpecification());


			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}
}