<?php
 /**
 * AuthController.php
 *
 */
//module/Application/src/Application/Controller/AuthController.php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Model\User;
use Application\Form\LoginForm;
use Zend\Crypt\Password\Bcrypt;


class AuthController extends AbstractActionController
{
	protected $storage;
	protected $authservice;

	public function getAuthService()
	{
		if (! $this->authservice) {
			$this->authservice = $this->getServiceLocator()
				->get('AuthService');
		}

		return $this->authservice;
	}

	public function getSessionStorage()
	{
		if (! $this->storage) {
			$this->storage = $this->getServiceLocator()
				->get('Application\Model\MyAuthStorage');
		}

		return $this->storage;
	}

	public function loginAction()
	{
		//if already login, redirect to success page
		if ($this->getAuthService()->hasIdentity()){
			return $this->redirect()->toRoute('success');
		}
		$form       = new LoginForm();
		$request = $this->getRequest();
		$this->flashmessenger()->clearMessages();
		if ($request->isPost()){
			$user = new User();
			$form->setInputFilter($user->getInputFilter());
			$form->setData($request->getPost());
			if ($form->isValid()){
				//check authentication...
				$bcrypt = new Bcrypt();
				$config = $this->getServiceLocator()->get('Config');
				$bcrypt->setSalt($config['vars']['salt']);
				$this->getAuthService()->getAdapter()
					->setIdentity($request->getPost('username'))
					->setCredential($bcrypt->create($request->getPost('password')));

				$result = $this->getAuthService()->authenticate();
				foreach($result->getMessages() as $message)
				{
					$this->flashmessenger()->addMessage($message);
				}
				if ($result->isValid()) {
					//check if it has rememberMe :
					if ($request->getPost('rememberme') == 1 ) {
						$this->getSessionStorage()->setRememberMe(1);
						//set storage again
						$this->getAuthService()->setStorage($this->getSessionStorage());
					}
					$this->getAuthService()->getStorage()->write($request->getPost('username'));
					return $this->redirect()->toRoute('success');
				}
			}
		}

		return array(
			'form'      => $form,
			'messages'  => $this->flashmessenger()->getMessages()
		);
	}

	public function logoutAction()
	{
		$this->getSessionStorage()->forgetMe();
		$this->getAuthService()->clearIdentity();
		return $this->redirect()->toRoute('home');
	}
	public function successAction()
	{
		if (!$this->getServiceLocator()->get('AuthService')->hasIdentity()){
			return $this->redirect()->toRoute('login');
		}
		return new ViewModel();
	}
}