<?php

class LookupTest extends CDbTestCase
{
	public $fixtures = array(
		'lookup' => 'Lookup'
	);

	public function testFixture()
	{
		$this->assertEquals(4, Lookup::model()->count());
		$this->assertEquals($this->lookup(4), Lookup::model()->findByPk(4));
	}

	public function testSave()
	{
		$lookup = Lookup::model()->findByPk(5);
		$this->assertNotNull($lookup);
		$this->assertEquals('Approved', $lookup->name);
		$newName      = "New name";
		$lookup->name = $newName;
		$this->assertEquals($newName, $lookup->name);
		$this->assertTrue($lookup->save());
		$this->assertEquals($newName, Lookup::model()->findByPk(5)->name);


	}
}
