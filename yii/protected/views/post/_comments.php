<?php foreach($comments as $comment): ?>
<div class="comment" id="c<?php echo $comment->id; ?>">

	<?php echo CHtml::link("#{$comment->id}", $comment->getUrl($post), array(
		'class'=>'cid',
		'title'=>'Permalink to this comment',
	)); ?>
	<span class="muted">
		<?php echo date('F j, Y \a\t h:i a',$comment->create_time); ?>
	</span><br/>
	<span class="author">
		<strong><?php echo $comment->authorLink; ?></strong> says:
	</span><br/>
	<p class="content">
		<?php echo nl2br(CHtml::encode($comment->content)); ?>
	</p>

</div><!-- comment -->
<?php endforeach; ?>