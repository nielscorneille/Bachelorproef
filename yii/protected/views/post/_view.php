<div class="post">
	<h3 style="margin-bottom: 0px"><?php echo CHtml::link(CHtml::encode($data->title), $data->url); ?></h3>
	<p class="muted">
		posted by <?php echo '<strong>' . $data->author->username . '</strong> on ' . date('F j, Y', $data->create_time); ?>
	</p>
	<div>
		<?php
		$this->beginWidget('CMarkdown', array('purifyOutput' => true));
		echo $data->content;
		$this->endWidget();
		?>
	</div>
	<div class="nav">
		<b>Tags:</b>
		<?php
		foreach ($data->getTagLinks() as $tag)
		{
			echo "<a href=" . $tag['link'] . ">";
			$this->widget('bootstrap.widgets.TbLabel', array(
				'type'  => $tag['label'],
				'label' => $tag['tag']
			));
			echo "</a> ";
		} ?>
		<br/>
		<?php echo CHtml::link('Permalink', $data->url); ?> |
		<?php echo CHtml::link("Comments ({$data->commentCount})", $data->url . '#comments'); ?> |
		Last updated on <?php echo date('F j, Y', $data->update_time); ?>
	</div>
</div>
