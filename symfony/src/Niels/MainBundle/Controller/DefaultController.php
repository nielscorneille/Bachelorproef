<?php

namespace Niels\MainBundle\Controller;
use Niels\MainBundle\Form\ContactForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Blank;
use Symfony\Component\Validator\Constraints\Collection;

class DefaultController extends Controller
{
	public function indexAction()
	{
		return $this->render('MainBundle:Default:index.html.twig');
	}

	/**
	 * @Template("MainBundle:Default:about.html.twig")
	 */
	public function aboutAction()
	{
		return array();

	}

	public function contactAction(Request $request)
	{
		$collectionConstraint = new Collection(array(
			'fields'           => array(
				'name'    => new NotBlank(),
				'email'   => new Email(array('message' => 'Invalid email address')),
				'message' => new Length(array('min' => 5)),
			),
			'allowExtraFields' => TRUE
		));
		$defaultData          = array('message' => 'Type your message here');
		$form                 = $this->createFormBuilder($defaultData, array(
			'validation_constraint' => $collectionConstraint,
		))
			->add('subject', 'text', array('required' => FALSE))
			->add('email', 'email')
			->add('name', 'text')
			->add('message', 'textarea')
			->add('captcha', 'captcha')
			->getForm();
		if ($request->getMethod() == 'POST')
		{
			$form->bindRequest($request);
			if ($form->isValid())
			{
				$data    = $form->getData();
				$message = \Swift_Message::newInstance()
					->setSubject($data['subject'])
					->setFrom($data['email'], $data['name'])
					->setTo($this->container->getParameter('mailTo'))
					->setBody($data['message']);
				$this->get('mailer')->send($message);
				return $this->render('MainBundle:Default:success.html.twig');
			}
		}
		return $this->render('MainBundle:Default:contact.html.twig', array(
			'form' => $form->createView()
		));

	}
}
