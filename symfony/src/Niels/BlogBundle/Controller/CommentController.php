<?php

namespace Niels\BlogBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Niels\BlogBundle\Entity\Comment;
use Niels\BlogBundle\Form\CommentType;

/**
 * Comment controller.
 *
 */
class CommentController extends Controller
{
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();
		$entities = $em->getRepository('BlogBundle:Comment')->findAll();

		return $this->render('BlogBundle:Comment:index.html.twig', array(
			'entities' => $entities,
		));
	}
    /**
     * Creates a new Comment entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Comment();
        $form = $this->createForm(new CommentType(), $entity);
        $form->bind($request);
        if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();
			$post_id = $request->query->get('id');
			$entity->setPost($em->getRepository('BlogBundle:Post')->find($post_id));


			$entity->setStatus($this->get('security.context')->isGranted('ROLE_USER')?Comment::STATUS_APPROVED:Comment::STATUS_PENDING);
			$entity->setCreateTime(time());

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('post_show', array('id' => $post_id)));
        }

        return $this->render('BlogBundle:Comment:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Comment entity.
     *
     */
    public function newAction()
    {
        $entity = new Comment();
		$entity->setAuthor($this->getUser());
        $form   = $this->createForm(new CommentType(), $entity);

        return $this->render('BlogBundle:Comment:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

	public function editAction($id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('BlogBundle:Comment')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Comment entity.');
		}

		$editForm = $this->createForm(new CommentType(), $entity);

		return $this->render('BlogBundle:Comment:edit.html.twig', array(
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
		));
	}

	/**
	 * Edits an existing Comment entity.
	 *
	 */
	public function updateAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('BlogBundle:Comment')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Comment entity.');
		}

		$editForm = $this->createForm(new CommentType(), $entity);
		$editForm->bind($request);

		if ($editForm->isValid()) {
			$em->persist($entity);
			$em->flush();

			return $this->redirect($this->generateUrl('comment_list'));
		}

		return $this->render('BlogBundle:Comment:edit.html.twig', array(
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
		));
	}

    /**
     * Deletes a Comment entity.
     *
     */
	public function deleteAction($id)
	{
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('BlogBundle:Comment')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Comment entity.');
		}

		$em->remove($entity);
		$em->flush();

		return $this->redirect($this->generateUrl('comment_list'));
	}

	public function approveAction($id)
	{
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('BlogBundle:Comment')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Comment entity.');
		}
		$entity->setStatus(Comment::STATUS_APPROVED);
		$em->persist($entity);
		$em->flush();

		return $this->redirect($this->generateUrl('comment_list'));
	}

}
