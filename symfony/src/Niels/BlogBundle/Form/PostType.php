<?php

namespace Niels\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('content')
            ->add('tags',null,array(
				'help_inline' => 'Please separate different tags with commas.'))//
            ->add('status', 'choice', array(
				'choices' =>
				array(1 => 'Draft', 2 => 'Published', 3 => 'Archived'),
			));
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Niels\BlogBundle\Entity\Post'
        ));
    }

    public function getName()
    {
        return '';
    }
}
