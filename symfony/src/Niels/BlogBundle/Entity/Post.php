<?php

namespace Niels\BlogBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblPost
 *
 * @ORM\Table(name="tbl_post")
 * @ORM\Entity
 */
class Post
{

	const STATUS_DRAFT     = 1;
	const STATUS_PUBLISHED = 2;
	const STATUS_ARCHIVED  = 3;
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @param \Blog\Model\Application\Model\User $author
	 */
	public function setAuthor($author)
	{
		$this->author = $author;
	}

	/**
	 * @return \Blog\Model\Application\Model\User
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * @param string $content
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}

	/**
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param int $createTime
	 */
	public function setCreateTime($createTime)
	{
		$this->createTime = $createTime;
	}

	/**
	 * @return int
	 */
	public function getCreateTime()
	{
		return $this->createTime;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return int
	 */
	public function getStatus()
	{
		return $this->status;
	}
	/**
	 * @return int
	 */
	public function getStatusLabel()
	{
		switch($this->status){
			case self::STATUS_ARCHIVED:
				return 'Archived';
			case self::STATUS_DRAFT:
				return 'Draft';
			case self::STATUS_PUBLISHED:
				return 'Published';
			default:
				return $this->status;
		}
	}

	/**
	 * @param string $tags
	 */
	public function setTags($tags)
	{
		$this->tags = $tags;
	}

	/**
	 * @return string
	 */
	public function getTags()
	{
		return $this->tags;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param int $updateTime
	 */
	public function setUpdateTime($updateTime)
	{
		$this->updateTime = $updateTime;
	}

	/**
	 * @return int
	 */
	public function getUpdateTime()
	{
		return $this->updateTime;
	}

	/**
	 * @var string
	 *
	 * @ORM\Column(name="title", type="string", length=128, nullable=false)
	 */
	private $title;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="content", type="text", nullable=false)
	 */
	private $content;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="tags", type="text", nullable=true)
	 */
	private $tags;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="status", type="integer", nullable=false)
	 */
	private $status;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="create_time", type="integer", nullable=true)
	 */
	private $createTime;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="update_time", type="integer", nullable=true)
	 */
	private $updateTime;

	/**
	 * @var \User
	 *
	 * @ORM\ManyToOne(targetEntity="User")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="author_id", referencedColumnName="id")
	 * })
	 */
	private $author;

	/**
	 * @var \Comment
	 *
	 * @ORM\OneToMany(targetEntity="Comment",mappedBy="post")
	 */
	private $comments;

	public $tagLinks;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add comments
     *
     * @param \Niels\BlogBundle\Entity\Comment $comments
     * @return Post
     */
    public function addComment(\Niels\BlogBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \Niels\BlogBundle\Entity\Comment $comments
     */
    public function removeComment(\Niels\BlogBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }
	/**
	 * Get approved comments
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getApprovedComments()
	{
		$approvedComments = array();
		foreach($this->getComments() as $comment){
			if($comment->isApproved()){
				$approvedComments[] = $comment;
			}
		}
		return $approvedComments;
	}
}
